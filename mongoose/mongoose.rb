require 'multi_json'
require_relative '../hedgehog/parse.rb'

TRUTHY = ['01', '10', '11' ]
WORDS = {
  '01' => 'MUST',
  '00' => 'MUST NOT',
  '10' => 'MAY',
  '11' => 'SHOULD',
}

def combine_scenarios(curr, latest)
  return latest unless curr

  latest.zip(curr).map do |tup|
    tup.first && tup.last
  end
end

class Equals
  def eval(doc)
    doc.fetch(key, nil) == val
  end
end

(doc_fn, rule_fn) = ARGV

doc = MultiJson.load(File.read(doc_fn))
(conds, asserts) = Parser.parse_file(rule_fn)

scenarios = nil
conds.each do |cond|
  matched = cond.expr.eval(doc)
  matching_scenarios = cond.scenarios.map do |sc|
    (matched && TRUTHY.include?(sc)) || (!matched && sc == '00')
  end

  scenarios = combine_scenarios(scenarios, matching_scenarios)
end

oughts = Array.new(scenarios.count { |sc| sc }, [])
oughts = asserts.reduce(oughts) do |oughts, ass|
  ass_scenarios = scenarios.zip(ass.scenarios).select(&:first).map(&:last)
  ass_oughts = ass_scenarios.map { |sc| { k: ass.key, v: ass.val, sc: sc } }
  oughts.zip(ass_oughts).map { |pr| pr.first + [pr.last] }
end

oughts.each do |ought|
  puts ought.map { |ass| "#{ass[:k]} #{WORDS[ass[:sc]]} #{ass[:v]}" }.join("\n")
  puts '------'
end

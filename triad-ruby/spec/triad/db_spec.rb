require('tmpdir')

require_relative('../../lib/triad')
require_relative('./rule_gen')
require_relative('./check_db')

RSpec.describe(Triad::DB) do
  include RuleGen
  include CheckDB

  it 'should make the db' do
    Dir.mktmpdir do |dn|
      db = Triad::DB.make(dn)
      expect(File.exist?(File.join(dn, 'index.db'))).to eql(true)
    end
  end

  it 'should add rules' do
    rules = generate_rules
    Dir.mktmpdir do |dn|
      db = Triad::DB.make(dn)
      rules.each { |rl| db.add(rl, '0.0.0') }
      check_db_contents(dn, rules, ['0.0.0'])
    end
  end
end

require('active_support')
require('active_support/core_ext/hash')

require_relative('../../lib/triad')
require_relative('./rule_gen')

RSpec.describe(Triad::Rules) do
  include RuleGen

  before(:all) do
    @asserts = generate_asserts(Faker::Number.within(range: 2..6)).map(&:to_h)
    @conds = generate_conds(Faker::Number.within(range: 2..6)).map(&:to_h)
  end

  it 'should convert an assertion to a hash' do
    @asserts.each do |c|
      a = Triad::Rules::Assertion.new(c[:key], c[:value], c[:scenarios])
      expect(a.to_h).to eql(c)
    end
  end

  it 'should create an assertion from a hash' do
    @asserts.each do |ah|
      a = Triad::Rules::Assertion.from_h(ah.with_indifferent_access)
      expect(a.to_h).to eql(ah)
    end
  end

  it 'should create a condition from a hash' do
    @conds.each do |ch|
      c = Triad::Rules::Condition.from_h(ch.with_indifferent_access)
      expect(c.to_h).to eql(ch)
    end
  end
end

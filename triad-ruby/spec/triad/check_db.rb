module CheckDB
  def check_db_contents(dir, rules, vers)
    db = SQLite3::Database.new(File.join(dir, 'index.db'))
    # avoid Triad::DB for checking the DB values
    ex_rows = rules.reduce([]) do |a, r|
      a + r.in_effect.reduce([]) do |aa, ie|
        aa + vers.map do |ver|
          [r.id, ver, ie[:in], ie[:from].iso8601, ie[:to].iso8601, ie[:tz]]
        end
      end
    end.sort_by { |it| [it[0], it[1], it[2], it[3]] }

    ac_rows = db.execute('
        SELECT rule_id, version, jurisdiction, from_t, to_t, tz
        FROM in_effect
        ORDER BY rule_id, version, jurisdiction, from_t')
    expect(ac_rows).to eql(ex_rows)

    ex_rows = rules.reduce([]) do |a, r|
      a + r.conditions.reduce(Set.new) do |s, cond|
        s << cond.expr.key
      end.reduce([]) do |a, k|
        a + vers.map do |ver|
          [r.id, ver, k]
        end
      end
    end.sort_by { |r| r }

    ac_rows = db.execute('
        SELECT rule_id, version, key
        FROM applicable
        ORDER BY rule_id, version, key')
    expect(ac_rows).to eql(ex_rows)
  end
end

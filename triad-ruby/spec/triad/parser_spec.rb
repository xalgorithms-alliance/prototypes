require 'time'
require_relative '../../lib/triad'

RSpec.describe Triad::Parse do
  it 'parses in_effect' do
    parser = Triad::Parse.new
    expect(parser.in_effect).to be_empty

    parser.accept("IN EFFECT\n")
    parser.accept("  IN CA-QC, FROM 2021-04-01T00:00, TO 2022-03-31T23:59, TZ Canada/Eastern\n")
    parser.accept("  IN CA-NS, FROM 2021-04-01T00:00, TO 2022-03-31T23:59, TZ Canada/Atlantic\n")
    parser.accept("\n")

    expect(parser.in_effect).not_to be_empty
    expect(parser.in_effect.count).to eql(2)
    expect(parser.in_effect[0]).to eql(
                                     :in   => 'CA-QC',
                                     :from => Time.parse('2021-04-01T00:00'),
                                     :to   => Time.parse('2022-03-31T23:59'),
                                     :tz   => 'Canada/Eastern',
                                   )
    expect(parser.in_effect[1]).to eql(
                                     :in   => 'CA-NS',
                                     :from => Time.parse('2021-04-01T00:00'),
                                     :to   => Time.parse('2022-03-31T23:59'),
                                     :tz   => 'Canada/Atlantic',
                                   )
  end

  it 'parses properties' do
    parser = Triad::Parse.new
    expect(parser.properties).to be_empty

    parser.accept("PROPERTIES\n")
    parser.accept("  ID e059d4b8-a992-4da9-aa2f-8dafcd335cba\n")
    parser.accept("  FOO not parsed, ignored\n")

    expect(parser.properties).not_to be_empty
    expect(parser.properties).to eql(id: 'e059d4b8-a992-4da9-aa2f-8dafcd335cba')
  end

  it 'parses equals conditions' do
    parser = Triad::Parse.new

    expect(parser.conditions).to be_empty

    cases = [
      {
        input: "container_status='loaded':         [00, 11, 00, 01, 01, 01, 00, 10]\n",
        key: 'container_status',
        val: 'loaded',
        scenarios: ['00', '11', '00', '01', '01', '01', '00', '10'],
      },
      {
        input: "validation='inspected':            [00, 11, 11, 00, 01, 00, 01, 01]\n",
        key: 'validation',
        val: 'inspected',
        scenarios: ['00', '11', '11', '00', '01', '00', '01', '01'],
      },
      {
        input: "door_status='locked':              [11, 01, 00, 01, 00, 00, 01, 01]\n",
        key: 'door_status',
        val: 'locked',
        scenarios: ['11', '01', '00', '01', '00', '00', '01', '01'],
      },
    ]

    cases.each do |c|
      parser.accept(c[:input])
    end

    expect(parser.conditions).to be_empty

    parser.accept("CONDITIONS\n")
    cases.each do |c|
      parser.accept(c[:input])
    end

    cases.each_with_index do |c, i|
      expect(parser.conditions[i].expr).to be_kind_of(Triad::Expressions::Equals)
      expect(parser.conditions[i].expr.key).to eq(c[:key])
      expect(parser.conditions[i].expr.value).to eq(c[:val])
      expect(parser.conditions[i].scenarios).to eq(c[:scenarios])
    end
  end

  it 'parses assertions' do
    parser = Triad::Parse.new

    expect(parser.assertions).to be_empty
    cases = [
      {
        :input     => "  risk_code: 'green':                [01, 01, 00, 00, 00, 00, 01, 01]\n",
        :key       => 'risk_code',
        :value     => 'green',
        :scenarios => ['01', '01', '00', '00', '00', '00', '01', '01'],
      },
      {
        :input     => "  risk_code: 'yellow':               [00, 00, 01, 00, 01, 01, 00, 00]\n",
        :key       => 'risk_code',
        :value     => 'yellow',
        :scenarios => ['00', '00', '01', '00', '01', '01', '00', '00'],
      },
      {
        :input     => "  risk_code: 'red':                  [00, 00, 00, 01, 00, 00, 00, 00]\n",
        :key       => 'risk_code',
        :value     => 'red',
        :scenarios => ['00', '00', '00', '01', '00', '00', '00', '00'],
      },
      {
        :input     => "  stevedore: 'notified':             [11, 10, 11, 01, 01, 01, 00, 10]\n",
        :key       => 'stevedore',
        :value     => 'notified',
        :scenarios => ['11', '10', '11', '01', '01', '01', '00', '10'],
      },
      {
        :input     => "  insurance: 'errors_and_omissions': [00, 01, 01, 10, 01, 00, 00, 01]\n",
        :key       => 'insurance',
        :value     => 'errors_and_omissions',
        :scenarios => ['00', '01', '01', '10', '01', '00', '00', '01'],
      },
    ]
  end
end

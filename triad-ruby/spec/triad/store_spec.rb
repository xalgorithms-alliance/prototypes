require('tmpdir')

require_relative('../../lib/triad')
require_relative('./rule_gen')
require_relative('./check_db')

RSpec.describe(Triad::Store) do
  include RuleGen
  include CheckDB

  before(:all) do
    @rules = generate_rules
  end

  def check_version(dn, ver, rules)
    rule_id = File.basename(dn)
    fn = File.join(dn, "#{ver}.json")
    expect(File.exist?(fn)).to eql(true)
    expect(JSON.parse(File.read(fn))).to eql(rules[rule_id].to_h.with_indifferent_access)
  end

  it 'should keep and update submitted rules' do
    Dir.mktmpdir do |dir|
      by_id = @rules.reduce({}) { |h, r| h.merge(r.id => r) }
      st = Triad::Store.new(dir)

      @rules.each { |r| st.keep(r) }

      ac = Dir[File.join(dir, '*')].map { |sub_dn| File.basename(sub_dn) }.select { |fn| fn != 'index.db' }
      ex = @rules.map(&:id)
      expect(ac.sort).to eql(ex.sort)
      expect(st.list.sort).to eql(ex.sort)

      ac.each do |dn|
        check_version(File.join(dir, dn), '0.0.1', by_id)
      end

      @rules.each { |r| st.update(r) }

      ac.each do |dn|
        check_version(File.join(dir, dn), '0.0.2', by_id)
      end

      check_db_contents(dir, @rules, ['0.0.1', '0.0.2'])
    end
  end
end

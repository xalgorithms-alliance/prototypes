require('active_support')
require('active_support/core_ext/hash')
require('active_support/core_ext/numeric')
require('countries')
require('faker')
require('time')
require('tzinfo')

module RuleGen
  def generate_asserts(n_scenarios)
    Faker::Number.within(range: 3..8).times.map { generate_assert(n_scenarios) }
  end

  def generate_assert(n_scenarios)
    Triad::Rules::Assertion.new(
      Faker::Lorem.word,
      Faker::Lorem.word,
      n_scenarios.times.map { Faker::Number.binary(digits: 2).to_s }
    )
  end

  def generate_conds(n_scenarios)
    Faker::Number.within(range: 3..8).times.map { generate_cond(n_scenarios) }
  end

  def generate_cond(n_scenarios)
    Triad::Rules::Condition.new(
      Triad::Expressions::Equals.new(
        Faker::Lorem.word,
        Faker::Lorem.word
      ),
      n_scenarios.times.map { Faker::Number.binary(digits: 2).to_s }
    )
  end

  def generate_rules
    Faker::Number.within(range: 3..8).times.map { generate_rule }
  end

  def generate_rule_parts
    @countries ||= ISO3166::Country.all

    country = @countries.sample
    subdivision = country.subdivisions.keys.sample
    n_scenarios = Faker::Number.within(range: 2..6)
    in_effect = Faker::Number.within(range: 2..4).times.map do
      tz = country.timezones.zones.sample
      {
        :in   => subdivision ? "#{country.alpha2}-#{subdivision}" : country.alpha2,
        :from => Faker::Time.backward(days: 14),
        :to   => Faker::Time.forward(days: 14),
        :tz   => tz ? tz.identifier : 'Canada/Atlantic',
      }
    end
    conds = generate_conds(n_scenarios)
    asserts = generate_asserts(n_scenarios)

    [in_effect, conds, asserts]
  end

  def generate_rule
    parts = generate_rule_parts
    Triad::Rules::Rule.new(*parts, { :id => Faker::Internet.uuid })
  end
end

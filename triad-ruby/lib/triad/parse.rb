require('time')

module Triad
  class Val
    MATCHES = {
      /^'(.+)'$/               => ->(vs) { String(vs) },
      /^([0-9]+)$/             => ->(vs) { Integer(vs) },
      /^([0-9](?:\.[0-9]+)?)$/ => ->(vs) { Float(vs) },
    }

    def self.parse(s)
      MATCHES.keys.each do |re|
        m = re.match(s)
        return MATCHES[re].(m[1]) if m
      end
    end
  end

  class KeyVal
    def initialize(keys)
      @keys = keys
    end

    def parse(s)
      s.strip.split(/,\s+/).reduce({}) do |h, ss|
        (k, v) = ss.split(/\s+/)
        k = k.downcase.to_sym
        @keys.key?(k) ? h.merge(k => @keys[k].call(v)) : h
      end
    end
  end

  class Parse
    attr_reader(:in_effect)
    attr_reader(:conditions)
    attr_reader(:assertions)
    attr_reader(:properties)

    PARSE_LIST   = ->(s) { s.split(/,\s*/) }
    PARSE_STRING = ->(s) { s }
    PARSE_DATE   = ->(s) { Time.parse(s) }
    # unite these with Val?

    def self.parse_file(fn)
      parser = Parse.new

      File.readlines(fn).each do |ln|
        parser.accept(ln)
      end

      Rules::Rule.new(parser.in_effect, parser.conditions, parser.assertions, parser.properties)
    end

    def initialize
      @state = method(:state_default)
      @conditions = []
      @assertions = []
      @in_effect = []
      @properties = {}
    end

    def accept(ln)
      return if comment?(ln)

      @state = @state.call(ln)
    end

    IN_EFFECT_KEY_PARSERS = {
      :in   => PARSE_STRING,
      :from => PARSE_DATE,
      :to   => PARSE_DATE,
      :tz   => PARSE_STRING,
    }

    PROPERTIES_KEY_PARSERS = {
      :id => PARSE_STRING,
    }

    private

    def next_state(ln)
      case ln.chomp
      when 'IN EFFECT'
        method(:state_in_effect)
      when 'CONDITIONS'
        method(:state_conditions)
      when 'ASSERTIONS'
        method(:state_assertions)
      when 'PROPERTIES'
        method(:state_properties)
      else
        @state
      end
    end

    def comment?(ln)
      ln =~ /\s*\#.*/
    end

    def state_default(ln)
      next_state(ln)
    end

    def state_in_effect(ln)
      @iek ||= KeyVal.new(IN_EFFECT_KEY_PARSERS)
      h = @iek.parse(ln)
      @in_effect << h unless h.empty?
      state_default(ln)
    end

    def state_properties(ln)
      @pk ||= KeyVal.new(PROPERTIES_KEY_PARSERS)
      @properties.merge!(@pk.parse(ln))
      state_default(ln)
    end

    def state_conditions(ln)
      m = /^\s*(.+)\:\s*\[(.+)\]\s*$/.match(ln)
      if m
        (expr, scenarios) = m[1..2]
        @conditions << Rules::Condition.new(
          Expressions::Expr.parse(expr), scenarios.split(/\s*\,\s*/)
        )
      end
      state_default(ln)
    end

    def state_assertions(ln)
      m = /^\s*(.+)\:\s*(.+)\s*\:\s*\[(.+)\]\s*$/.match(ln)
      if m
        (k, v, scenarios) = m[1..3]
        @assertions << Rules::Assertion.new(k, Val.parse(v), scenarios.split(/\s*\,\s*/))
      end
      state_default(ln)
    end
  end
end

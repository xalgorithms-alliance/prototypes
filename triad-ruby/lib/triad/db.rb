require 'fileutils'
require 'set'
require 'sqlite3'

require_relative 'parse'

module Triad
  class DB
    RULES_DB = 'index.db'

    class << self
      def make(dir)
        db_fn = File.join(dir, RULES_DB)
        FileUtils.mkdir_p(dir)
        create = File.exist?(db_fn)
        db = SQLite3::Database.new(db_fn)
        unless create
          db.execute <<~SQL
            CREATE TABLE in_effect (
              id           INTEGER PRIMARY KEY AUTOINCREMENT,
              rule_id      text,
              version      text,
              jurisdiction text,
              from_t       text,
              to_t         text,
              tz           text
            );
          SQL
          db.execute <<~SQL
            CREATE TABLE applicable (
              id      int  PRIMARY KEY,
              rule_id text,
              version text,
              key     text
            );
          SQL
        end

        DB.new(db)
      end
    end

    def initialize(db)
      @db = db
    end

    def add(rl, version)
      rl.in_effect.each do |ie|
        @db.execute(
          'INSERT INTO in_effect (rule_id, version, jurisdiction, from_t, to_t, tz) VALUES (?, ?, ?, ?, ?, ?)',
          rl.id,
          version,
          ie[:in],
          ie[:from].iso8601,
          ie[:to].iso8601,
          ie[:tz]
        )
      end

      rl.conditions.reduce(Set.new) do |s, c|
        s << c.expr.key
      end.each do |k|
        @db.execute(
          'INSERT INTO applicable (rule_id, version, key) VALUES (?, ?, ?)',
          rl.id,
          version,
          k
        )
      end
    end
  end
end

require('sinatra/base')
require('json')

require_relative('../triad/expressions')
require_relative('../triad/db')
require_relative('../triad/rules')
require_relative('../triad/sessions')
require_relative('../triad/store')

module Triad
  class API < Sinatra::Application
    not_found do
      { status: :not_found }.to_json
    end

    get('/') do
      { version: '0.0.0' }.to_json
    end

    post('/rules') do
      st = Store.new
      o = JSON.parse(request.body.read)

      rule_id = o.dig('properties','id')

      unless rule_id
        status(400)
        return {
          reason: :rule_id_not_provided,
        }.to_json
      end

      if st.exists?(rule_id)
        status(403)
        return {
          reason: :rule_exists,
          rule_id: rule_id,
        }.to_json
      end

      ver = st.keep(Rules::Rule.from_h(o))
      { rule_id: rule_id, version: ver }.to_json
    end

    get('/rules') do
      # list rules
      st = Store.new
      st.list.to_json
    end

    put('/rules/:id') do
      st = Store.new
      o = JSON.parse(request.body.read)

      rule_id = params[:id]

      unless st.exists?(rule_id)
        status(404)
        return {
          reason: :rule_not_found,
          rule_id: rule_id,
        }.to_json
      end

      ver = st.update(Rules::Rule.from_h(o))
      { rule_id: rule_id, version: ver }.to_json
    end

    get('/rules/:id') do
      # list versions
      st = Store.new
      rule_id = params['id']

      unless st.exists?(rule_id)
        status(404)
        return {
          reason: :rule_not_found,
          rule_id: rule_id,
        }.to_json
      end

      {
        rule_id: rule_id,
        versions: st.versions(params['id']),
      }.to_json
    end

    get('/rules/:id/:version') do
      # single version
      rule_id = params['id']
      ver = params['version']

      st = Store.new

      unless st.exists?(rule_id)
        status(404)
        return { reason: :rule_not_found, rule_id: rule_id, version: ver }.to_json
      end

      s = st.read(rule_id, ver)
      unless s
        status(404)
        return { reason: :version_not_found, rule_id: rule_id, version: ver }.to_json
      end

      s
    end

    post('/sessions') do
      s = Triad::Sessions::Session.make
      return { id: s.id }.to_json
    end

    get('/sessions') do
      Triad::Sessions.list.map(&:id).to_json
    end

    get('/sessions/:id') do
      sess = Triad::Sessions.load(params['id'])
      unless sess
        status(404)
        return { id: params['id'] }.to_json
      end

      sess.rules.map(&:id).to_json
    end

    get('/sessions/:session_id/:rule_id') do
      sess = Triad::Sessions.load(params['session_id'])
      unless sess
        status(404)
        return { reason: :session_not_found, session_id: params['session_id'] }.to_json
      end

      rule = sess.rules.find { |r| r.id == params['rule_id'] }
      unless rule
        status(404)
        return {
          reason: :rule_not_found,
          session_id: params['session_id'],
          rule_id: params['rule_id']
        }.to_json
      end

      rule.copies.map { |cp| cp.time.to_i }.to_json
    end

    get('/sessions/:session_id/:rule_id/:copy_id') do
      sess = Triad::Sessions.load(params['session_id'])
      unless sess
        status(404)
        return { reason: :session_not_found, session_id: params['session_id'] }.to_json
      end

      rule = sess.rules.find { |r| r.id == params['rule_id'] }
      unless rule
        status(404)
        return {
          reason: :rule_not_found,
          session_id: params['session_id'],
          rule_id: params['rule_id']
        }.to_json
      end

      cp = rule.copies.find { |cp| cp.time.to_i.to_s == params['copy_id'] }
      unless cp
        status(404)
        return {
          reason: :copy_not_found,
          session_id: params['session_id'],
          rule_id: params['rule_id'],
          copy_id: params['copy_id'],
        }.to_json
      end

      cp.content.to_json
    end

    put('/sessions/:session_id/:rule_id') do
      sess = Triad::Sessions.load(params['session_id'])
      unless sess
        status(404)
        return { reason: :session_not_found, id: params['session_id'] }
      end

      o = JSON.parse(request.body.read)

      rule = Triad::Sessions::Rule.new(sess.path, params['rule_id'])
      copy_id = rule.add(o)

      { id: copy_id }.to_json
    end

    run! if app_file === $0
  end
end

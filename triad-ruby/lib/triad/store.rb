require('fileutils')
require('json')

module Triad
  class Store
    PATH = 'rules'

    def initialize(path=PATH)
      @path = path
      @db = DB.make(@path)
    end

    def exists?(rule_id)
      File.exist?(rule_path(rule_id))
    end

    def rule_path(rule_id)
      File.join(@path, rule_id)
    end

    def rule_file_path(rule_id, ver)
      File.join(rule_path(rule_id), "#{ver}.json")
    end

    def list
      Dir[File.join(@path, '*')].map { |fn| File.basename(fn) }.select { |fn| fn != Triad::DB::RULES_DB }
    end

    def versions(rule_id)
      Dir[File.join(rule_path(rule_id), '*')].map { |fn| File.basename(fn, '.json') }
    end

    def keep(rule, rule_id=nil)
      ver = '0.0.1'

      rule_id = rule.id unless rule_id
      fn = rule_file_path(rule_id, ver)
      FileUtils.mkdir_p(File.dirname(fn))
      File.write(fn, rule.to_h.to_json)

      @db.add(rule, ver)

      ver
    end

    def read(rule_id, ver)
      fn = rule_file_path(rule_id, ver)

      return unless File.exist?(fn)

      File.read(fn)
    end

    def update(rule)
      rule_id = rule.id
      ver = next_version(rule_id)

      fn = rule_file_path(rule_id, ver)
      File.write(fn, rule.to_h.to_json)

      @db.add(rule, ver)

      ver
    end

    def next_version(rule_id)
      vers = Dir[File.join(rule_path(rule_id), '*')].map do |fn|
        ver_s = File.basename(fn, '.json')
        (_, _, patch) = ver_s.split('.')
        patch.to_i
      end.sort

      "0.0.#{vers.last + 1}"
    end
  end
end

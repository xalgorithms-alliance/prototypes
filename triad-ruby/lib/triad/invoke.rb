module Triad
  class Invoke
    TRUTHY = ['01', '10', '11' ]

    class << self
      def combine_scenarios(curr, latest)
        return latest unless curr

        latest.zip(curr).map do |tup|
          tup.first && tup.last
        end
      end

      def call(doc, conds, asserts)
        scenarios = nil
        conds.each do |cond|
          matched = cond.expr.eval(doc)
          matching_scenarios = cond.scenarios.map do |sc|
            (matched && TRUTHY.include?(sc)) || (!matched && sc == '00')
          end

          scenarios = combine_scenarios(scenarios, matching_scenarios)
        end

        oughts = Array.new(scenarios.count { |sc| sc }, [])
        asserts.reduce(oughts) do |oughts, ass|
          ass_scenarios = scenarios.zip(ass.scenarios).select(&:first).map(&:last)
          ass_oughts = ass_scenarios.map { |sc| { k: ass.key, v: ass.val, sc: sc } }
          oughts.zip(ass_oughts).map { |pr| pr.first + [pr.last] }
        end
      end
    end
  end
end

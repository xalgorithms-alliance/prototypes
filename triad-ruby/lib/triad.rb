module Triad
  autoload(:DB,          'triad/db')
  autoload(:Expressions, 'triad/expressions')
  autoload(:Invoke,      'triad/invoke')
  autoload(:Parse,       'triad/parse')
  autoload(:Rules,       'triad/rules')
  autoload(:Sessions,    'triad/sessions')
  autoload(:Store,       'triad/store')
end

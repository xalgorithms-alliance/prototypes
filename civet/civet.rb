require 'date'
require 'fileutils'
require 'listen'
require 'multi_json'
require 'pathname'
require 'sequel'
require 'set'

create = File.exist?('./rules.db')
db = Sequel.connect('sqlite://rules.db')
unless create
  db.create_table(:in_effect) do
    primary_key :id
    String      :rule_id
    String      :jurisdiction
    DateTime    :from
    DateTime    :to
    String      :tz
  end

  db.create_table(:applies) do
    primary_key :id
    String      :rule_id
    String      :key
  end
end

submit_dn = File.expand_path(ARGV[0] || './submit')
artifacts_dn = File.expand_path(ARGV[1] || './artifacts')
rules_dn = File.expand_path(ARGV[2] || './rules')
resolutions_dn = File.expand_path(ARGV[3] || './resolutions')

queues = {
  'documents' => -> fn {
    puts "+ retaining #{File.basename(fn)}"
    FileUtils.cp(fn, File.join(artifacts_dn, File.basename(fn)))
  },
  'rules' => -> fn {
    puts "+ inserting #{File.basename(fn)}"
    o = MultiJson.load(File.read(fn))
    eff = o['effective']

    common = {
      rule_id: o['id'],
      from: eff.key?('from') ? DateTime.parse(eff['from']) : nil,
      to: eff.key?('to') ? DateTime.parse(eff['to']) : nil,
      tz: eff.fetch('tz', nil),
    }
    eff.fetch('in', []).each do |juris|
      db[:in_effect] << common.merge(jurisdiction: juris)
    end

    o.fetch('input_conditions', []).reduce(Set.new) do |s, ic|
      s << ic['key']
    end.each do |k|
      db[:applies] << { rule_id: o['id'], key: k }
    end

    FileUtils.cp(fn, File.join(rules_dn, "#{o['id']}.ior"))
  },
  'content_ids' => -> fn {
  },
  'resolutions' => -> fn {
    o = MultiJson.load(File.read(fn))
    docs = o['documents'].reduce({}) do |h, fn|
      h.merge(fn => MultiJson.load(File.read(File.join(artifacts_dn, fn))))
    end
    dt = Date.today.to_time
    to_invoke = {}
    db[:in_effect]
      .where(jurisdiction: o['jurisdiction'])
      .all
      .select { |rie| rie[:from] <= dt && rie[:to] >= dt }
      .each do |rie|
      applies_keys = db[:applies].where(rule_id: rie[:rule_id]).all.map { |r| r[:key] }
      to_invoke = docs.reduce({}) do |h, (doc, contents)|
        h.merge(doc => h.fetch(doc, []) + [rie[:rule_id]])
      end
    end

    to_invoke.each do |doc, rule_ids|
      doc_dn = File.join(resolutions_dn, doc)
      FileUtils.mkdir_p(doc_dn)
      rule_ids.each do |rule_id|
        fn = "#{rule_id}.ior"
        FileUtils.ln_s(File.join(rules_dn, fn), File.join(doc_dn, fn))
      end
    end
  }
}

dirs = queues.keys.map { |n| File.join(submit_dn, n) }
dirs.each do |dn|
  FileUtils.mkdir_p(dn)
end
FileUtils.mkdir_p(artifacts_dn)
FileUtils.mkdir_p(rules_dn)


l = Listen.to(*dirs) do |mods, adds, dels|
  adds.each do |fn|
    rpn = Pathname.new(fn).relative_path_from(submit_dn)
    queues[File.dirname(rpn)].(fn)
    FileUtils.rm(fn)
  end
end
l.start

sleep

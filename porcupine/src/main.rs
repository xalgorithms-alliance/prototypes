use regex::Regex;
use std::fs::File;
use std::io::BufRead;
use std::env;

struct State {
    accept: fn(&str, &State) -> State,
}

fn is_comment(ln: &str) -> bool {
    match Regex::new(r"^\s*\#.*$") {
        Ok(re) => re.is_match(ln),
        _ => false,
    }
}


fn parse_line(ln: &str, state: &State) -> State {
    if is_comment(ln) {
        return clone_state(state);
    }

    return (state.accept)(ln, state);
}

fn clone_state(state: &State) -> State {
    return State { accept: state.accept };
}

fn next_state(ln: &str, state: &State) -> State {
    return match ln {
        "CONDITIONS" => State { accept: state_conditions },
        "ASSERTIONS" => State { accept: state_assertions },
        _ => clone_state(state),
    }
}

fn state_default(ln: &str, state: &State) -> State {
    return next_state(ln, state);
}

fn state_conditions(ln: &str, state: &State) -> State {
    println!("conds");
    return state_default(ln, state)
}

fn state_assertions(ln: &str, state: &State) -> State {
    println!("asserts");
    return state_default(ln, state);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let path = &args[1];
    if let Ok(f) = File::open(path) {
        let mut state = State { accept: state_default };
        for rres in std::io::BufReader::new(f).lines() {
            if let Ok(ln) = rres {
                state = parse_line(ln.trim(), &state)
            }
        }
    }
}
